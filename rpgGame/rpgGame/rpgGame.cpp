// rpgGame.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <Windows.h>

using namespace std;

char Map[10][10] = {"#########",
					"#  #   !#",
					"#  # ####",
					"####*#  #",
					"#    #  #",
					"# ####  #",
					"# #     #",
					"# #     #",
					"#@#     #",
					"#########" };

int Gamespeed = 100;
int Level = 1;
bool stopgame = false;
int HP = 100;
int maxHP = 100;

void getKey(int key, int y1, int y2, int x1, int x2)
{
	if (GetAsyncKeyState(key) != 0)
	{
		int y2 = (y1 - 1);
		int x2 = (x1 - 1);

		switch (Map[y2][x1])
		{
		case ' ':
		{
			Map[y1][x1] = ' ';
			y1 -= 1;
			Map[y2][x1] = '@';
		}break;
		case '!':
		{
			Level = 2;
		}break;
		default:
			break;
		}
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	
	while (stopgame == false && Level == 1)
	{
		system("cls");
		for (int y = 0; y < 10; y++)
		{
			cout << Map[y] << endl;
		}
		cout << "HP: " << HP << "/" << maxHP << endl;
		for (int y = 0; y < 10; y++)
		{
			for (int x = 0; x < 10; x++)
			{
				switch (Map[y][x])
				{
				case '#':
				{
					Map[y][x] = 219;
				}
				case '@':
				{
					if (GetAsyncKeyState(VK_UP) != 0)
					{
						int y2 = (y - 1);

						switch (Map[y2][x])
						{
							case ' ':
							{
								Map[y][x] = ' ';
								y -= 1;
								Map[y2][x] = '@';
							}break;
							case '!':
							{
								Level = 2;
							}break;
							case '*':
							{
								HP -= 20;
								y -= 1;
								Map[y2][x] = '@';
							}break;
						}
					}
					if (GetAsyncKeyState(VK_DOWN) != 0)
					{
						int y2 = (y + 1);

						switch (Map[y2][x])
						{
						case ' ':
						{
							Map[y][x] = ' ';
							y += 1;
							Map[y2][x] = '@';
						}break;
						case '!':
						{
							Level = 2;
						}break;
						{
							HP -= 20;
							y += 1;
							Map[y2][x] = '@';
						}break;
						default:
							break;
						}
					}
					if (GetAsyncKeyState(VK_RIGHT) != 0)
					{
						int x2 = (x + 1);

						switch (Map[y][x2])
						{
						case ' ':
						{
							Map[y][x] = ' ';
							x += 1;
							Map[y][x2] = '@';
						}break;
						case '!':
						{
							Level = 2;
						}break;
						{
							HP -= 20;
							x += 1;
							Map[y][x2] = '@';
						}break;
						default:
							break;
						}
					}
					if (GetAsyncKeyState(VK_LEFT) != 0)
					{
						int x2 = (x - 1);

						switch (Map[y][x2])
						{
						case ' ':
						{
							Map[y][x] = ' ';
							x -= 1;
							Map[y][x2] = '@';
						}break;
						case '!':
						{
							Level = 2;
						}break;
						{
							HP -= 20;
							x -= 1;
							Map[y][x2] = '@';
						}break;
						default:
							break;
						}
					}
				}
				default:
					break;
				}
			}
		}
		Sleep(Gamespeed);
	}

	while (stopgame == false && Level == 2)
	{
		system("cls");
		cout << "Level 2 script goes here...\n\n";
		system("pause");
		return EXIT_SUCCESS;
	}

	return 0;
}

