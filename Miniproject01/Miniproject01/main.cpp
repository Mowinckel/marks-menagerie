#include "opencv2/opencv.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <stdlib.h>
#include <stdio.h>

using namespace std;
using namespace cv;

int absVal(int v)
{
	return v*((v < 0)*(-1) + (v > 0));
}

int sobelCorrelation(Mat InputArray, int x, int y, String xory)
{ 
	int edgeCount = 0;
	
	if (xory == "x") {
		edgeCount = InputArray.at<uchar>(y - 1, x - 1) * 0 +
			InputArray.at<uchar>(y - 1, x) * 1 +
			InputArray.at<uchar>(y - 1, x + 1) * 2 +
			InputArray.at<uchar>(y, x - 1) * (-1) +
			InputArray.at<uchar>(y, x) * 0 +
			InputArray.at<uchar>(y, x + 1) * 1 +
			InputArray.at<uchar>(y + 1, x - 1) * (-2) +
			InputArray.at<uchar>(y + 1, x) * (-1) +
			InputArray.at<uchar>(y + 1, x + 1) * 0;
		return edgeCount;
	}
	else if (xory == "y")
	{
		edgeCount = InputArray.at<uchar>(y - 1, x - 1) * (-2) +
			InputArray.at<uchar>(y - 1, x) * (-1) +
			InputArray.at<uchar>(y - 1, x + 1) * 0 +
			InputArray.at<uchar>(y, x - 1) * (-1) +
			InputArray.at<uchar>(y, x) * 0 +
			InputArray.at<uchar>(y, x + 1) * 1 +
			InputArray.at<uchar>(y + 1, x - 1) * 0 +
			InputArray.at<uchar>(y + 1, x) * 1 +
			InputArray.at<uchar>(y + 1, x + 1) * 2;
		return edgeCount;
	}
	else
	{
		return 0;
	}


}

Mat sobelLocalisation(Mat InputArray)
{
	int sum, gx, gy;
	Mat dst = InputArray.clone();

	for (int y = 0; y < InputArray.rows; y++)
		for (int x = 0; x < InputArray.cols; x++)
			dst.at<uchar>(y, x) = 0.0;

	for (int y = 1; y < InputArray.rows - 1; ++y) {
		for (int x = 1; x < InputArray.cols - 1; ++x){
			gx = sobelCorrelation(InputArray, x, y, "x");
			gy = sobelCorrelation(InputArray, x, y, "y");
			sum = absVal(gx) + absVal(gy);
			if (sum > 255)
				sum = 255;
			else if (sum < 0)
				sum = 0;
			dst.at<uchar>(y, x) = sum;
		}
	}
	return dst;
}

Mat applyThreshold(Mat image, int thresholdValue)
{

	for (int y = 0; y < image.rows; ++y) {
		for (int x = 0; x < image.cols; ++x){
			int value = image.at<unsigned char>(y, x);
			if (value >= thresholdValue){
				image.at<unsigned char>(y, x) = 255;
			}
			else {
				image.at<unsigned char>(y, x) = 0;
			}
		}
	}

	return image;
}

void imageOutput(Mat image, String path) {
	image = imread(path, CV_LOAD_IMAGE_GRAYSCALE);
	Mat output;
	if (image.data && !image.empty()){

		Mat blurred; 
		GaussianBlur(image, blurred, Size(5,5), 0, 0);
		
		output = sobelLocalisation(blurred);
		
		namedWindow("Original");
		imshow("Original", image);

		namedWindow("Gaussian");
		imshow("Gaussian", blurred);

		namedWindow("Diagonal Edges");
		imshow("Diagonal Edges", output);

		Mat thresholded = applyThreshold(output, 128);

		imshow("Thresholded", thresholded);
	}
	waitKey(0);
}

int main(int argc, char* argv[]) {
	
	Mat image;

	imageOutput(image, "C:/Dropbox/lena.jpg");
	return 0;
}